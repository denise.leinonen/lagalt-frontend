import keycloak from "../../keycloak";
import {useState, useEffect} from "react";
import FetchApi from "../../fetch/FetchApi";
import Navbar from "../../navbar/Navbar";


function LoginForm() {
    const [isOpen, setOpen] = useState(false);
    const [isOpen1, setOpen1] = useState(false);
    // useEffect(() => {
    //     FetchApi.createUser(keycloak.idTokenParsed.sub)
    //         .then(() => keycloak.tokenParsed['preferred_username'])
    // }, []);


    const handleTokenJson = () => {
        setOpen(true);
    }

    const handleTokenParsed = () => {
        setOpen1(true);
    }
    return (
        <div>
            {/* <h1>Start Page</h1>*/}
            {keycloak.authenticated && (
                <div>
                    <button onClick={handleTokenJson}>ID Token Json</button>
                    {isOpen &&
                        <div>
                            <div>sub : {keycloak.idTokenParsed.sub}</div>
                            <div>realm_access : {keycloak.idTokenParsed.realm_access}</div>
                            <div>iat : {keycloak.idTokenParsed.iat}</div>
                            <div>resource_access : {keycloak.idTokenParsed.resource_access}</div>
                            <div>session_state : {keycloak.idTokenParsed.session_state}</div>
                            <div>azp : {keycloak.idTokenParsed.azp}</div>
                            <div>nonce : {keycloak.idTokenParsed.nonce}</div>
                            <div>acr : {keycloak.idTokenParsed.acr}</div>
                            <div>aud : {keycloak.idTokenParsed.aud}</div>
                            <div>exp : {keycloak.idTokenParsed.exp}</div>
                            <div>amr : {keycloak.idTokenParsed.amr}</div>
                            <div>iss : {keycloak.idTokenParsed.iss}</div>
                            <div>preferred_username : {keycloak.tokenParsed['preferred_username']}</div>
                            <div>email : {keycloak.tokenParsed['email']}</div>
                            <div>name : {keycloak.tokenParsed['name']}</div>

                        </div>
                    }
                    {isOpen1 &&
                        <div>

                        </div>
                    }
                </div>

            )}
            {
                keycloak.token &&
                <div>{keycloak.token}</div>
            }
        </div>
    );
}

export default LoginForm;