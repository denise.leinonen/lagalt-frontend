import React from 'react';
import {useKeycloak} from "@react-keycloak/web";

const MyProjectInfo = ({getProjectInfo}) => {
    const {keycloak} = useKeycloak()


    return (
        <div>
            <p>{getProjectInfo.projectUrl}</p>
            <p>{getProjectInfo.projectTypes}</p>
            <p>{getProjectInfo.title}</p>
            <p>{getProjectInfo.description}</p>
        </div>
    );
};

export default MyProjectInfo;