import React from 'react';
import {useKeycloak} from "@react-keycloak/web";

const ProjectFollowsRequestInfo = ({getProjectFollowsRequest, getAllProjectToFollow}) => {
    const {keycloak} = useKeycloak();
    const approveFollowRequest = async () => {
        await fetch(`https://app-lagalt-bakend.herokuapp.com/api/v1/user/approve-user`, {
            method: 'PUT',
            headers: {
                'Authorization': 'Bearer ' + keycloak.token,
                'content-type': 'application/json'
            },
            body: JSON.stringify({projectId: getProjectFollowsRequest.projectId}
            ),
        });
        getAllProjectToFollow()
    }
    return (
        <div>
            {getProjectFollowsRequest.title}
            {getProjectFollowsRequest.requestToFollow.map((request,index) => (
                <ol key={index}>
                    <div>
                        <div>{request.username}</div>
                        <div>{request.email}</div>
                        <button className="bg-blue-400 text-white active:bg-blue-600 font-bold uppercase
                                         text-sm px-2 py-2 rounded shadow hover:shadow-lg hover:bg-blue-600 outline-none
                                          focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                            onClick={approveFollowRequest}><a href="/project-follows-request">approve</a></button>
                    </div>

                </ol>

            ))}
        </div>
    );
};

export default ProjectFollowsRequestInfo;