
import './App.css';
import { ReactKeycloakProvider } from "@react-keycloak/web";


import {
  BrowserRouter,
  Routes,
  Route
} from 'react-router-dom'
import StartPage from './views/StartPage';
import KeycloakRoute from "./Router/KeycloakRoute";
import UserPage from "./views/UserPage";
import Navbar from "./navbar/Navbar";
import keycloak from "./keycloak";
import {useEffect} from "react";
import CreateProjectPage from "./views/CreateProjectPage";
import MyProjectPage from "./views/MyProjectPage";
import MyProjectInfo from "./components/MyProjectInfo";
import AllProjects from "./views/AllProjects";
import ProjectFollowsRequestInfo from "./components/ProjectFollowsRequestInfo";
import ProjectFollowsRequest from "./views/ProjectFollowsRequest";
import SentRequest from "./views/SentRequest";



function App() {

  return (
      <div>
      <ReactKeycloakProvider authClient={keycloak}>
          <Navbar/>
          <BrowserRouter><div className="App">

           <Routes>

        <Route path="/" element={ <StartPage/> } />
        <Route path="/create-project" element={ <CreateProjectPage/> } />
        <Route path="/user-projects" element={ <MyProjectPage/> } />
        <Route path="/project-follows-request" element={ <ProjectFollowsRequest/> } />
        <Route path="/sent-request" element={ <SentRequest/> } />
        <Route path="/project-info" element={ <MyProjectInfo/> } />
        <Route path="/all-projects" element={ <AllProjects/> } />
        <Route path="/profile" element={<UserPage/>} />

            </Routes>
        </div></BrowserRouter>
      </ReactKeycloakProvider>
      </div>
  );
}

export default App;