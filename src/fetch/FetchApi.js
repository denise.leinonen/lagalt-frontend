import keycloak from "../keycloak";
import {useKeycloak} from "@react-keycloak/web";



  export const createUserAPI = () =>
      fetch(`https://app-lagalt-bakend.herokuapp.com/api/v1/user/register`, {
          method: 'POST',
          headers: {
              'Authorization': 'Bearer ' + keycloak.token,
              'content-type': 'application/json'
          },
      })


    export const getAllProjectsAPI =  () =>
       fetch(`https://app-lagalt-bakend.herokuapp.com/api/v1/project/getAllProjects`, {
            method: 'GET',
            headers: {
                // 'Authorization': 'Bearer ' + keycloak.token,
                'Content-Type': 'application/json;'
            },
        })


    export const getProjectFollowsRequestAPI = (id) =>
       fetch('https://app-lagalt-bakend.herokuapp.com/api/v1/user/get-one-project/' + id , {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + keycloak.token,
                'content-type': 'application/json'
            },
        })


    export const getAllProjectToFollowAPI = () =>
        fetch(`https://app-lagalt-bakend.herokuapp.com/api/v1/project/getAllProjectToFollow/` + keycloak.tokenParsed.sub, {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + keycloak.token,
                'content-type': 'application/json'
            },
        })


    export const getAllFollowedProjectsAPI = () =>
        fetch(`https://app-lagalt-bakend.herokuapp.com/api/v1/project/getAllFollowedProjects/` + keycloak.tokenParsed.sub, {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + keycloak.token,
                'content-type': 'application/json'
            },
        })


    export const usersProjectsAPI = () =>
        fetch(`https://app-lagalt-bakend.herokuapp.com/api/v1/user/users-projects/` + keycloak.tokenParsed.sub, {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + keycloak.token,
                'content-type': 'application/json'
            },
        })

// export const getOneUserAPI = () =>
//     fetch(`http://localhost:8080/api/v1/user/get-one-user/` + keycloak.tokenParsed.sub, {
//         method: 'GET',
//         headers: {
//             'Authorization': 'Bearer ' + keycloak.token,
//             'content-type': 'application/json'
//         },
//     })
// export const getOneUserAPI = () =>
//     fetch(`http://localhost:8080/api/v1/user/get-one-user/` + keycloak.tokenParsed.sub, {
//         method: 'GET',
//         headers: {
//             'Authorization': 'Bearer ' + keycloak.token,
//             'content-type': 'application/json'
//         },
//     })


    export const getProjectInfoAPI = (id) =>
        fetch('https://app-lagalt-bakend.herokuapp.com/api/v1/user/get-one-project/' + id, {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + keycloak.token,
                'content-type': 'application/json'
            },
        })


    export const getAllProjectsForLoggedInAPI = () =>
        fetch(`https://app-lagalt-bakend.herokuapp.com/api/v1/project/getAllProjectsForLoggedIn/` + keycloak.tokenParsed.sub, {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + keycloak.token,
                'content-type': 'application/json'
            },
        })






