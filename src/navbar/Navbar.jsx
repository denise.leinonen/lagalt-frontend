import {Link, Navigate, useNavigate} from "react-router-dom";
import {useKeycloak} from "@react-keycloak/web";

const Navbar = () => {
    const { keycloak, initialized } =  useKeycloak()

    return (
        <div>
            <div className="flex mb-10 bg-white bg-opacity-70 top-0 w-full flex-wrap sticky top-0 z-50 justify-between">
                <section className="h-24">
                    <nav className=" text-blue-800 w-screen mb-10">
                        <div className="px-5 xl:px-12 py-6 flex w-full items-center">
                            <h1 className="text-3xl font-bold font-heading">
                                Lagalt
                            </h1>
                            <ul className="hidden md:flex px-4 mx-auto font-semibold font-heading space-x-10">
                                    <li>
                                        <a className="hover:text-blue-800" href="/">
                                            Home
                                        </a>
                                    </li>

                                {keycloak.authenticated && (
                                    <ul className={"flex justify-between"}>
                                        <li>
                                            <a className="hover:text-blue-800 mr-10" href="/profile">
                                                User Page
                                            </a>
                                        </li>
                                        <li>
                                            <a className="hover:text-blue-800 mr-10" href="/user-projects">
                                                My Projects
                                            </a>
                                        </li>

                                        <li>
                                            <a className="hover:text-blue-800 mr-10" href="/project-follows-request">
                                                Following Requests
                                            </a>
                                        </li>

                                        <li>
                                            <a className="hover:text-blue-800 mr-10" href="/sent-request">
                                                Sent Request
                                            </a>
                                        </li>

                                        <li>
                                            <a className="hover:text-blue-800 mr-10"  href="/all-projects">
                                                All Projects
                                            </a>
                                        </li>
                                    </ul>

                                )

                                }

                            </ul>
                            <div className="flex justify-between color-blue-800">
                                <div className="hover:text-gray-200">
                                    {!keycloak.authenticated && (
                                        <>
                                        <button
                                            type="button"
                                            className="text-blue-800 mx-4"
                                            onClick={() => keycloak.login({
                                                redirectUri: 'https://app-lagalt-front.herokuapp.com/profile',
                                            })}
                                        >
                                            Login
                                        </button>
                                        <button
                                        type="button"
                                        className="text-blue-800"
                                        onClick={() => keycloak.register({
                                            redirectUri: 'https://app-lagalt-front.herokuapp.com/profile',
                                        })}
                                        >
                                        register
                                        </button>
                                        </>
                                    )}

                                    {keycloak.authenticated && (

                                        <button
                                            type="button"
                                            className="text-blue-800"
                                            onClick={() => keycloak.logout()}
                                        >
                                            <svg className="h-8 w-8 text-blue-500" width="24" height="24"
                                                 viewBox="0 0 24 24" strokeWidth="2" stroke="currentColor" fill="none"
                                                 strokeLinecap="round" strokeLinejoin="round">
                                                <path stroke="none" d="M0 0h24v24H0z"/>
                                                <circle cx="9" cy="7" r="4"/>
                                                <path d="M3 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2"/>
                                                <path d="M16 11l2 2l4 -4"/>
                                            </svg>({keycloak.tokenParsed.preferred_username})
                                        </button>

                                    )}
                                </div>
                            </div>
                        </div>
                    </nav>
                </section>
            </div>
        </div>
    );
}

export default Navbar;