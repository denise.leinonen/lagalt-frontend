import {useKeycloak} from "@react-keycloak/web";
import {Navigate} from "react-router-dom";

const withAuth = Component => props => {
    const {keycloak} = useKeycloak()
    if (keycloak.authenticated) {
        return <Component {...props} />
    } else {
        return <Navigate to="/" />
    }
}

export default withAuth;