import {useKeycloak} from "@react-keycloak/web";
function KeycloakRoute({ children }) {

    const { keycloak } = useKeycloak();

    const isLoggedIn = keycloak.authenticated;

    return isLoggedIn ? children : null;

}

export default KeycloakRoute;

