import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { initialize } from './keycloak';
const root = ReactDOM.createRoot(document.getElementById('root'));
// dhgeerergerg
initialize()
    .then(() => {
        root.render(
            <div>
                <App />
            </div>
        );
    })
    .catch(() => {
        root.render(
            <div>
                <p>Could Not Connect To Keycloak.</p>
            </div>
        );
    });

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
