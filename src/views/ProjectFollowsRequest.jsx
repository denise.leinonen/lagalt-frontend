import React, {useEffect, useState} from 'react';
import {useKeycloak} from "@react-keycloak/web";
import withAuth from "../Router/withAuth";
import ProjectFollowsRequestInfo from "../components/ProjectFollowsRequestInfo";
import {getAllProjectToFollowAPI, getProjectFollowsRequestAPI} from "../fetch/FetchApi";

const ProjectFollowsRequest = () => {
    const {keycloak} = useKeycloak()
    const [projectsList, setProjectsList] = useState([
        {projectId: "",projectUrl: "", description: '', projectTypes: "", title: ''}
    ])
    const [project, setProject] = useState(
        {projectId: "",projectUrl: "", description: '', projectTypes: "", title: '',
            requestToFollow: []}
    )

    const getProjectFollowsRequest = (id) => {
       getProjectFollowsRequestAPI(id)
           .then(res => res.json())
            .then(response => {
                setProject(response)
            });
    }

    const getAllProjectToFollow = () => {
       getAllProjectToFollowAPI()
           .then(res => res.json())
            .then(data =>{
                setProjectsList(data)
                console.log(data)
            })
    }



    useEffect(() => {
       getAllProjectToFollow()
    }, []);

    return (
        <div>
            {
                projectsList.map((p) => (

                    <ol key={p.projectId} >
                        <div className="max-w-sm bg-white rounded overflow-hidden shadow-lg mx-auto">
                            <div className="px-6 py-4">{p.title}
                                <div className="font-bold text-xl mb-2">
                                    <button className="bg-slate-400 text-white active:bg-slate-600 font-bold uppercase
                                         text-sm px-8 py-3 rounded shadow hover:shadow-lg hover:bg-slate-600 outline-none focus:outline-none
                                         mr-1 mb-1 ease-linear transition-all duration-150" onClick={() =>
                                        getProjectFollowsRequest(p.projectId)}>info</button>
                                </div>
                                <div className="text-gray-700 text-base">
                                    {
                                        project.projectId === p.projectId &&
                                        <ProjectFollowsRequestInfo getAllProjectToFollow={getAllProjectToFollow} getProjectFollowsRequest={project}/>

                                    }
                                </div>
                            </div>
                        </div>
                    </ol>
                ))
            }

        </div>

    );
};

export default withAuth(ProjectFollowsRequest);