import React, {useEffect, useState} from 'react';

import {useKeycloak} from "@react-keycloak/web";
import LoginForm from "../components/Login/LoginForm";
import {useNavigate} from "react-router-dom";
import Navbar from "../navbar/Navbar";
import withAuth from "../Router/withAuth";
import {createUserAPI} from "../fetch/FetchApi";

const UserPage = () => {
    //TODO när man har registrerat eller loggat in så ska man komma till userpage, vi måste använda useEffect här
    const {keycloak} = useKeycloak()
    const userData = {
        uid: keycloak.idTokenParsed.sub,
        firstname: keycloak.tokenParsed.given_name,
        lastname: keycloak.tokenParsed.family_name,
        email: keycloak.tokenParsed.email,
        username: keycloak.tokenParsed.preferred_username,
        roles: keycloak.tokenParsed.roles,
        requestB: false
    }
    const navigate = useNavigate();


    useEffect(() => {
         createUserAPI().then(r => {
                console.log(r)
             console.log(userData.requestB)
            })
    }, []);

    const createProject = () => {
        navigate("/create-project")
    }

    return (

        <>
            <section className="bg-blueGray-50">
                <div className="w-full lg:w-4/12 sm:w-50 mx-auto">
                    <div
                        className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-lg mt-2">
                        <div className="px-2">
                            <div className="text-center mt-12">
                                <h3 className="text-xl font-semibold leading-normal mb-2 text-blueGray-700 mb-2">
                                    {userData.firstname} {userData.lastname}
                                </h3>
                                <div className="text-sm leading-normal mt-0 mb-2 text-blueGray-400 font-bold uppercase">
                                    <i className="fas fa-map-marker-alt mr-2 text-lg text-blueGray-400"></i>
                                    {userData.username}

                                </div>
                                <div className="mb-2 text-blueGray-600 mt-10">
                                    <i className="fas fa-briefcase mr-2 text-lg text-blueGray-400"></i>
                                    <button onClick={createProject}
                                            className="bg-blue-500 hover:bg-blue-400 text-white font-bold py-2 px-4 border-b-4 border-blue-700 hover:border-blue-500 rounded">create
                                        project
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer className="relative  pt-8 pb-6 mt-8">
                    <div className="container mx-auto px-4">
                        <div className="flex flex-wrap items-center md:justify-between justify-center">
                            <div className="w-full md:w-6/12 px-4 mx-auto text-center">
                            </div>
                        </div>
                    </div>
                </footer>
            </section>

        </>
    )
}


export default withAuth(UserPage);