import React, {useEffect, useState} from 'react';
import keycloak from "../keycloak";
import {useKeycloak} from "@react-keycloak/web";
import withAuth from "../Router/withAuth";

const SentRequest = () => {
    const {keycloak} = useKeycloak()
    const [projectsList, setProjectsList] = useState([
        {projectId: "", projectUrl: "", description: '', projectTypes: "", title: '', request: false,
            requestToFollow: [{uid: '', firstname: ''}]
        }
    ])
    const getAllProjectsWithSentRequest = () => {
        fetch(`https://app-lagalt-bakend.herokuapp.com/api/v1/project/getAllProjectsWithSentRequest/` + keycloak.tokenParsed.sub, {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + keycloak.token,
                'content-type': 'application/json'
            },
        }).then(res => res.json())
            .then(response => {
                setProjectsList(response)
            })
    }
    useEffect(() => {
     getAllProjectsWithSentRequest()
    }, []);
    return (
        <div>
            {
                projectsList.map((p, index) => (
                    <ol key={index}>

                        <div className="max-w-sm bg-white rounded overflow-hidden shadow-lg mx-auto">
                            <div className="px-6 py-4">
                                <div>
                                    {p.projectUrl}
                                </div>
                                <div>
                                    {p.projectTypes}
                                </div>
                                <div>
                                    {p.title}
                                </div>
                                <div>
                                    {p.description}
                                </div>
                            </div>
                        </div>
                    </ol>
                ))
            }

        </div>
    );
};

export default withAuth(SentRequest);