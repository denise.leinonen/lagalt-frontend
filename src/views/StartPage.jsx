import React, {useEffect, useState} from "react";
import {useKeycloak} from "@react-keycloak/web";
import MyProjectInfo from "../components/MyProjectInfo";
import {TextField} from "@mui/material";
import SearchBar from "./SearchBar";
import {getAllProjectsAPI} from "../fetch/FetchApi";

const StartPage = () => {
    const [user, setUser] = useState()
    const {keycloak} = useKeycloak()
    const [title, setTitle] = useState('')
    const [projectsList, setProjectsList] = useState([
        {projectId: "", projectUrl: "", description: '', projectTypes: "", title: ''}
    ])

    const [project, setProject] = useState(
        {projectId: "", projectUrl: "", description: '', projectTypes: "", title: ''}
    )

    const updateSearchText = (item) => {
        setTitle(item)
    }

    const filteredData = projectsList.filter((el) => {
        //if no input the return the original
        if (title === '') {
            return el;
        }
        //return the item which contains the user input
        else {
            return el.title.toLowerCase().includes(title)
        }
    })


    useEffect(() => {
        getAllProjectsAPI()
            .then(res => res.json())
            .then(data => {
                setProjectsList(data)
                console.log(data)
            })
    }, [])


    return (
        <div>
            <SearchBar filteredData={updateSearchText}/>
            <div>
                {

                    filteredData.map((p, index) => (
                        <ol key={index}>
                            <div className="max-w-sm rounded overflow-hidden shadow-lg mx-auto bg-white mb-10">
                                <div className="px-6 py-4">
                                    <div>
                                        {p.title}
                                    </div>
                                    <div>
                                        {p.description}
                                    </div>
                                </div>
                            </div>
                        </ol>
                    ))
                }

            </div>

        </div>
    );
}

export default StartPage