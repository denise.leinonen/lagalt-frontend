import React, {useState} from 'react';
import {TextField} from "@mui/material";

const SearchBar = ({filteredData}) => {
    let inputHandler = (e) => {
    const {value} = e.target
        filteredData(value)
    };
    const searchFieldStyle = {
        width: "55%",
        margin: "0 auto",
        marginBottom: "20px"
    }
    return (
            <div className="search" style={searchFieldStyle}>
                <TextField
                    id="outlined-basic"
                    onChange={inputHandler}
                    variant="outlined"
                    style={searchFieldStyle}
                    label="Search"
                />
            </div>
    );
};

export default SearchBar;