import React, {useState} from 'react';
import {useKeycloak} from "@react-keycloak/web";
import {useNavigate} from "react-router-dom";

const CreateProjectPage = () => {
    const {keycloak} = useKeycloak()
    const [projectUrl, setProjectUrl] = useState("")
    const [projectTypes, setProjectTypes] = useState("")
    const [title, setTitle] = useState("")
    const [description, setDescription] = useState("")
    const navigate = useNavigate();

    const handleSubmit = async (e) => {
        e.preventDefault();
        let res = await fetch(`https://app-lagalt-bakend.herokuapp.com/api/v1/project/create-project/` + keycloak.tokenParsed.sub, {
            method: 'POST',
            headers: {
                'Authorization': 'Bearer ' + keycloak.token,
                'content-type': 'application/json'
            },
            body: JSON.stringify({
                projectUrl: projectUrl,
                projectTypes: projectTypes,
                title: title,
                description: description
            }),
        });
        if (res.status === 201) {
            setProjectUrl('')
            setProjectTypes('')
            setTitle('')
            setDescription('')
            navigate("/user-projects")
        }
    }

    return (

        <section className="dark:bg-gray-900">
            <div className="py-8 lg:py-16 px-4 mx-auto max-w-screen-md">
                <h2 className="mb-4 text-4xl tracking-tight font-extrabold text-center text-gray-900 dark:text-white">Create
                    projects
                </h2>
                <p className="mb-8 lg:mb-16 font-light text-center text-gray-500 dark:text-gray-400 sm:text-xl">Here can
                    you create and share your projects.</p>

                <form className="form-login space-y-8" onSubmit={handleSubmit} action="#">
                    <div>

                        <input
                            type="text "
                            value={projectUrl}
                            onChange={(e) => setProjectUrl(e.target.value)}
                            className="input-login shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500 dark:shadow-sm-light"
                            placeholder="projectUrl" required
                        />
                    </div>
                    <div>

                        <input
                            type="text"
                            value={projectTypes}
                            onChange={(e) => setProjectTypes(e.target.value)}
                            className="input-login shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500 dark:shadow-sm-light"
                            placeholder="projecType" required
                        />
                    </div>
                    <div>

                        <input
                            type="text"
                            value={title}
                            onChange={(e) => setTitle(e.target.value)}
                            className="input-login shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500 dark:shadow-sm-light"
                            placeholder="title" required
                        />
                    </div>
                    <div>

                        <input
                            type="text"
                            value={description}
                            onChange={(e) => setDescription(e.target.value)}
                            className="input-login shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500 dark:shadow-sm-light"
                            placeholder="description" required
                        />
                    </div>
                    <button type="submit"
                            className="bg-blue-500 hover:bg-blue-400 text-white font-bold py-2 px-4 border-b-4 border-blue-700 hover:border-blue-500 rounded">create
                    </button>

                </form>
            </div>
        </section>
    );
};

export default CreateProjectPage;