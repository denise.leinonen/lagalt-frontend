import React, {useEffect, useState} from "react";
import {useKeycloak} from "@react-keycloak/web";
import withAuth from "../Router/withAuth";
import SearchBar from "./SearchBar";
import {getAllProjectsForLoggedInAPI} from "../fetch/FetchApi";
import keycloak from "../keycloak";

const AllProjects = () => {
    const [user, setUser] = useState({
        uid: "", firstname: "", requestB: false
    })
    const [title, setTitle] = useState('')
    const {keycloak} = useKeycloak()
    const [projectsList, setProjectsList] = useState([
        {projectId: "", projectUrl: "", description: '', projectTypes: "", title: '', request: false,
            requestToFollow: [{uid: '', firstname: ''}]
        }
    ])

    const [project, setProject] = useState(
        {projectId: "", projectUrl: "", description: '', projectTypes: "", title: '', request: Boolean}
    )

    const [buttonRequest, setButtonRequest] = useState(false)

    const updateSearchText = (item) => {
        setTitle(item)
    }

    const filteredData = projectsList.filter((el) => {
        //if no input the return the original
        if (title === '') {
            return el;
        }
        //return the item which contains the user input
        else {
            return el.title.toLowerCase().includes(title)
        }
    })

    const getProjects = () => {
       getAllProjectsForLoggedInAPI()
           .then(res => res.json())
            .then(data => {
                setProjectsList(data)
                console.log(data)
            })
    }

    const getOneUser = () => {
        fetch(`https://app-lagalt-bakend.herokuapp.com/api/v1/user/get-one-user/` + keycloak.tokenParsed.sub, {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + keycloak.token,
                'content-type': 'application/json'
            }
        })
            .then(res => res.json())
            .then(response =>{
                setUser(response)
                console.log(response)
            } )
    }
    useEffect(() => {
     getOneUser()
    }, []);

    useEffect(() => {
        getProjects()
    }, []);

    const requestToFollowProject = async (id) => {

        let res = await fetch(`https://app-lagalt-bakend.herokuapp.com/api/v1/user/request-to-follow_project/` + keycloak.tokenParsed.sub, {
            method: 'POST',
            headers: {
                'Authorization': 'Bearer ' + keycloak.token,
                'content-type': 'application/json'
            },
            body: JSON.stringify({
                projectId: id
            }),
        })
        getProjects()
        getOneUser()

    }


    return (
        <div>
            <SearchBar filteredData={updateSearchText}/>
            <div>
                {

                    filteredData.map((p, index) => (
                        <ol key={index}>
                            <div className="max-w-sm bg-white rounded overflow-hidden shadow-lg mx-auto mb-4">
                                <div className="px-6 py-4">

                                        {/*// p.request === true && user.requestB === true ?*/}
                                        {/*//     <button disabled={buttonRequest} className="bg-gray-200">*/}
                                        {/*//         request sent</button> :*/}
                                            <button className="bg-blue-400 text-white active:bg-blue-600 font-bold uppercase
                                         text-sm px-4 py-3 rounded shadow hover:shadow-lg hover:bg-blue-600 outline-none focus:outline-none
                                         mr-1 mb-1 ease-linear transition-all duration-150"
                                                    onClick={() => requestToFollowProject(p.projectId)}>
                                                Send request</button>

                                    <div>
                                        {p.projectUrl}
                                    </div>
                                    <div>
                                        {p.projectTypes}
                                    </div>
                                    <div>
                                        {p.title}
                                    </div>
                                    <div>
                                        {p.description}
                                    </div>
                                </div>
                            </div>
                        </ol>
                    ))

                }

            </div>

        </div>
    );
}

export default withAuth(AllProjects)