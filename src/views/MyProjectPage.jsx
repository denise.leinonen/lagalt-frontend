import React, {useEffect, useState} from 'react';
import {useKeycloak} from "@react-keycloak/web";
import MyProjectInfo from "../components/MyProjectInfo";
import withAuth from "../Router/withAuth";
import SearchBar from "./SearchBar";
import {getAllFollowedProjectsAPI, getAllProjectsAPI, getProjectInfoAPI, usersProjectsAPI} from "../fetch/FetchApi";


const MyProjectPage = () => {
    const {keycloak} = useKeycloak()
    const [title, setTitle] = useState('')
    const [projectsList, setProjectsList] = useState([
        {projectId: "", projectUrl: "", description: '', projectTypes: "", title: ''}
    ])
    const [listOfAllProjects, setListOfAllProjects] = useState([
        {projectId: "", projectUrl: "", description: '', projectTypes: "", title: ''}
    ])

    const [followingProject, setFollowingProject] = useState([
        {projectId: "", projectUrl: "", description: '', projectTypes: "", title: ''}
    ])

    const [project, setProject] = useState(
        {
            projectId: "", projectUrl: "", description: '', projectTypes: "", title: '',
            requestToFollow: []
        }
    )
    const [projectUrl, setProjectUrl] = useState("")
    const [projectTypes, setProjectTypes] = useState("")
    const [projectTitle, setProjectTitle] = useState("")
    const [description, setDescription] = useState("")

    const updateSearchText = (item) => {
        setTitle(item)
    }

    const filteredData = projectsList.filter((el) => {
        //if no input the return the original
        if (title === '') {
            return el;
        }
        //return the item which contains the user input
        else {
            return el.title.toLowerCase().includes(title)
        }
    })

    const filterFollowingData = followingProject.filter((el) => {
        //if no input the return the original
        if (title === '') {
            return el;
        }
        //return the item which contains the user input
        else {
            return el.title.toLowerCase().includes(title)
        }
    })

    const getAllProjects = () => {
        getAllProjectsAPI()
            .then(res => res.json())
            .then(data => {
                setListOfAllProjects(data)
                console.log(data)
            })
    }

    const getAllFollowedProjects = () => {
        getAllFollowedProjectsAPI()
            .then(res => res.json())
            .then(data => {
                setFollowingProject(data)
                console.log(data)
            })
    }

    const usersProjects = () => {
        usersProjectsAPI()
            .then(res => res.json())
            .then(data => {
                setProjectsList(data)
                console.log(data)
            })
    }


    const getProjectInfo = (id) => {
        getProjectInfoAPI(id)
            .then(res => res.json())
            .then(response => {
                setProject(response)
            });
    }
    const updateProjectInfo = () => {
        fetch('https://app-lagalt-bakend.herokuapp.com/api/v1/user/update-project', {
            method: 'PUT',
            headers: {
                'Authorization': 'Bearer ' + keycloak.token,
                'content-type': 'application/json'
            },
            body: JSON.stringify({
                projectId: project.projectId,
                projectUrl: projectUrl,
                projectTypes: projectTypes,
                title: projectTitle,
                description: description
            }),
        }).then(res => res.json())
            .then(response => {
                setProject(response)
            });
        usersProjects()
    }

    useEffect(() => {
        getAllFollowedProjects()
    }, []);

    useEffect(() => {
        getAllProjects()
    }, []);

    useEffect(() => {
        usersProjects()

    }, []);

    const [showModal, setShowModal] = React.useState(false);
    return (
        <div>
            <SearchBar filteredData={updateSearchText}/>

            {showModal ? (
                <>
                    <div
                        className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
                    >
                        <div className="relative w-auto my-6 mx-auto max-w-3xl">
                            {/*content*/}
                            <div
                                className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                                {/*header*/}
                                <div
                                    className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                                    <h3 className="text-3xl font-semibold">
                                        Update
                                    </h3>
                                    <button
                                        className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                                        onClick={() => setShowModal(false)}
                                    >
                    <span
                        className="bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none">
                      ×
                    </span>
                                    </button>
                                </div>
                                {/*body*/}
                                <div className="relative p-6 flex-auto">
                                    <form className="form-login" action="#"
                                          className="space-y-8">

                                        <div>

                                            <input
                                                type="text"
                                                onChange={(e) => setProjectUrl(e.target.value)}
                                                className="input-login shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500 dark:shadow-sm-light"
                                                placeholder={project.projectUrl} required
                                            />
                                        </div>
                                        <div>

                                            <input
                                                type="text"
                                                onChange={(e) => setProjectTypes(e.target.value)}
                                                className="input-login shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500 dark:shadow-sm-light"
                                                placeholder={project.projectTypes} required
                                            />
                                        </div>
                                        <div>

                                            <input
                                                type="text"
                                                onChange={(e) => setProjectTitle(e.target.value)}
                                                className="input-login shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500 dark:shadow-sm-light"
                                                placeholder={project.title} required
                                            />
                                        </div>
                                        <div>

                                            <input
                                                type="text"
                                                onChange={(e) => setDescription(e.target.value)}
                                                className="input-login shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500 dark:shadow-sm-light"
                                                placeholder={project.description} required
                                            />
                                        </div>

                                    </form>
                                </div>
                                {/*footer*/}
                                <div
                                    className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
                                    <button
                                        className="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                                        type="button"
                                        onClick={() => setShowModal(false)}
                                    >
                                        Close
                                    </button>
                                    <button
                                        className="bg-emerald-500 text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                                        type="button"
                                        onClick={() => {
                                            updateProjectInfo()
                                            setShowModal(false)
                                            usersProjects()

                                        }}
                                    >
                                        Save Changes
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                </>
            ) : null}
            <div>
                {
                    filteredData.map((p) => (
                        <ol key={p.projectId}>
                            <div className="max-w-sm bg-white rounded overflow-hidden shadow-lg mx-auto mb-10">
                                <div className="px-6 py-4">{p.title}
                                    <div className="font-bold text-xl mb-2">
                                        <button className="bg-slate-400 text-white active:bg-slate-600 font-bold uppercase
                                         text-sm px-8 py-3 rounded shadow hover:shadow-lg hover:bg-slate-600 outline-none focus:outline-none
                                         mr-1 mb-1 ease-linear transition-all duration-150" onClick={() =>
                                            getProjectInfo(p.projectId)}>More info
                                        </button>
                                        <button
                                            className="bg-pink-500 text-white active:bg-pink-600 font-bold uppercase
                                            text-sm px-6 py-3 rounded shadow hover:shadow-lg hover:bg-pink-700 outline-none
                                             focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                                            type="button"
                                            onClick={() => {
                                                getProjectInfo(p.projectId)
                                                setShowModal(true)
                                            }}
                                        >
                                            Update
                                        </button>
                                    </div>
                                    <div className="text-gray-700 text-base">
                                        {
                                            project.projectId === p.projectId &&
                                            <MyProjectInfo getProjectInfo={project}/>
                                        }
                                    </div>
                                </div>
                            </div>
                        </ol>
                    ))
                }
            </div>
            <div>
                <h1>Following projects</h1>

                {
                    filterFollowingData.map((p) => (
                        <ol key={p.projectId}>
                            <div className="max-w-sm rounded overflow-hidden shadow-lg mx-auto">
                                <div className="px-6 py-4">
                                    <div>
                                        {p.title}
                                    </div>
                                    <div>
                                        {p.projectUrl}
                                    </div>
                                </div>
                            </div>
                        </ol>
                    ))
                }
            </div>

        </div>

    );
};

export default withAuth(MyProjectPage);