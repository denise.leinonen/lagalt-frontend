# Lagalt

* In this case, the candidates are supposed to come up with and build a distributed software solution.
The website contains a register and the sign in, which a user can register an account and then sign in. As a user:
* Create projects
* follow request project
* Approve request
* searchbar

The owner of the projects can approve the request, allowing the user to follow the owners projects.
there is also a search bar where users can search for a specific project.

## To open the project globally click on the following link
Open [https://app-lagalt-front.herokuapp.com](https://app-lagalt-front.herokuapp.com) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.



## Contributing
[Mohamed Abdel Monem (@Mohxmedzz)](@Mohxmedzz)
[Rinat Iunusov (@lynxxxxx)](@lynxxxxx)
[Denise Leinonen (@denise.leinonen)](@denise.leinonen)
[Hussein Mohamed (@huah1600.hmah)](@huah1600.hmah)


